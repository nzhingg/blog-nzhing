## Date

### Date对象(date)
- 常用API
1. 获取时间： getFullYear(),getMonth(),getDate(),getHours(),getMinutes(),getSeconds(),getDay()
2. 设置时间： setDate(),setHours(0,0,0,0)[整点]
- Date对象获取时间戳： 
1. Date.parse(date)
2. date.valueOf()
3. date.getTime()


### 时间戳
> 从1970年到现在的毫秒数
- 计算两个时间戳相差的天数问题： (stamp1 - stamp2) / 24 / 3600 / 1000
- 时间戳转化成Date对象： new Date(stamp)

